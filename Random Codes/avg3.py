#avg3.py
#   A program to find the average of three exams
#by: Elijah Iverson

def main():
    print("This program computes the average of three exam scores.")
    score1, score2, score3 = eval(input("Enter three scores separated by a comma: "))
    average = (score1 + score2 + score3)/3
    print("The average of the scores is:", average)
    input("Enter the <Enter> key to quit.")
