#text2numbers.py
#   A program to convert a sequence of Unicode numbers into
#       a string of text.

def main():
    print("This program converts a sequence of Unicode numbers into")
    print("the string of text that it represents.\n")

    #Get the message to encode
    inString = input("Please enter the Unicode-encoded message: ")

    #Loop through the message and print out the Unicode values
    message=""
    for numStr in inString.split():
        #convert the (sub)string to a number
        codeNum=int(numStr)
        # append characters to message
        message = message + chr(codeNum)

    print("\nThe decoded message is:",message)
