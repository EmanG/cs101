def maxValue(x1,x2,x3):
    if x1 >= x2 and x1 >= x3:
        return x1
    elif x2 >= x1 and x2 >= x3:
        return x2
    else:
        return x3

def main():
    x1 = float(input("Enter your first value: "))
    x2 = float(input("Enter your second value: "))
    x3 = float(input("Enter your third value: "))
    maxx = maxValue(x1,x2,x3)
    print(maxx,"is your greatest value!")
main()
