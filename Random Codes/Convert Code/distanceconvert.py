#distanceconvert.py
#   A program that converts kilometers to miles
#by: Elijah Iverson

def main():
    print("This program converts Kilometers to Miles")
    km = eval(input("Enter the distance measured in Kilometers: "))
    mi = round(km * 0.62,3)
    print(km," Kilometers is", mi," Miles")
    input("Press the <Enter> to quit.")
