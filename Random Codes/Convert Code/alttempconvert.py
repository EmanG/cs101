#alttempconvert.py
#   A program that converts Fahrenheit to Celsius.
#by: Elijah Iverson

def main():
    print("This program converts Fahrenheit to Celsius")
    for i in range (11):
        f=i*10
        c=round((f-32)*(5/9),2)
        print(f,"F =",c,"C")
    input("Press the <Enter> key to quit.")
