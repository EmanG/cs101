#intocmconvert.py
#   A program that converts inches to centimeters
#by: Elijah Iverson

def main():
    print("This program converts inches to centimeters")
    i = eval(input("Enter the amount of inches you have: "))
    cm = round(i * 2.54,2)
    print(i,"inches is", cm," centiemeters")
    input("Press the <Enter> key to quit.")
