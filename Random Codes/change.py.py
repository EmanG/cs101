#change.py
#   Aprogram to calculate the value of some change in dollars

def main():
    print("Change counter")
    print()
    print("Please enter the count of each coin type.")
    quarters = int(input("Quarters: "))
    dimes = int(input("Dimes: "))
    nickels = int(input("Nickels: "))
    pennies = int(input("Pennies: "))
    total = round((quarters * .25 + dimes * .1 + nickels * .05 + pennies * .01),2)
    print()
    print("The total value of your change is", total)
    input("Press the <Enter> key to quit.")
    
