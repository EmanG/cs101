#Graphic function examples

from graphics import *

def main():
    win = GraphWin()
    triCircle(win)
main()

def triCircle(win):
    redCircle(Point(50,50),30).draw(win)
    blueCircle(Point(100,100),60).draw(win)
    greenCircle(Point(75,150),30).draw(win)

def redCircle(p,r):
    Circle(p,r).setFill('red')

def blueCircle(p,r):
    Circle(p,r).setFill('blue')

def greenCircle(p,r):
    Circle(p,r).setFill('green')
