#average6.py

def main():
    fileName = input("What file are the numbers in? ")
    infile = open(fileName, 'r')
    sum = 0.0
    count = 0
    line = infile.readline()
    print("This is the first line): ", line)
    while line != "":
        sum += float(line)
        count += 1
        line = infile.readline()
        print(line)
    print("\nThe average of the numbers is", sum / count)
main()
