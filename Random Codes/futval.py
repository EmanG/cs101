#futval.py
#   A program to compute the value of an investment
#   carried 10 years into the future
#by: Elijah Iverson

def main():
    print("This program calculates the future value")
    print("of a 10-year investment.")
    base = eval(input("Enter the initial principal: "))
    periods = eval(input("Enter the number of times that the interest is compounded each year: "))
    rate = eval(input("Enter the interest rate: "))
    for i in range(10 * periods):
        base = round(base + rate/periods,2)
    print("The value in",periods," years is:", base)
    input("Press the <Enter> key to quit.")
    
