from graphics import *

def main():
    win = GraphWin('target',500,500)

    center = Point((250),(250))
    circle1 = Circle(center,100)
    circle1.setFill("white")
    circle2 = Circle(center, 80)
    circle2.setFill("black")
    circle3 = Circle(center, 60)
    circle3.setFill("blue")
    circle4 = Circle(center, 40)
    circle4.setFill("red")
    circle5 = Circle(center,20)
    circle5.setFill("yellow")
    circle1.draw(win)
    circle2.draw(win)
    circle3.draw(win)
    circle4.draw(win)
    circle5.draw(win)
    win.getMouse()
    win.close()
main()
