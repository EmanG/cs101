#number2text2
# a program to convert a sequence of unicode numbers into a string of text.
# Efficient version using a list accumulator

def main():
    print("This program converts a sequence of unicide numbers into")
    print("the string of text that it represents.\n")

    #Get the message to encode
    inString = input("Please enter the unicode-encoded message: ")

    #Loop through each substring and build Unicode message
    chars = []
    for numStr in inString.split():
        codeNum = int(numStr)
        chars.append(chr(codeNum))

    message = "".join(chars)
    print("\nthe decoded message is:", message)
main()
