#P5_16
#by:Eliah Iverson

from graphics import *

def main():
    file = input("Enter filename: ")
    infile = open(file,'r')
    win = GraphWin('Quiz Histogram',240,180)
    count = 0 #counter for temp-use for each number in the range(0-10)
    y =160 #y coordinate
    x =20  #x corrdinate
    data = infile.readlines()
    for num in range(0,11): #0,1,2,3,4,5,6,7,8,9,10
        Text(Point(x,y+10),num).draw(win) #draws the text on the GraphWin (eachnumber)
        for line in data:
            if line == str(num)+"\n":
                count+=1
        Rectangle(Point(x-5,y),Point(x+5,y-count*10)).draw(win) #draws the bars of the amount that the number below it shows up
        x+=20 #position-edit for the x coordinate
        count=0 #resets count for next number in for loop of i
        
    win.getMouse() #waits for mouse click to close the file and window
    infile.close()
    win.close()
main()

#C:\\Users\\eliiv\\Downloads\\Year1\\Fall\\CS-101\\Lab Codes\\Lab 5\\ForP5_16.py

    
