#P5_15
#by:Eliah Iverson

from graphics import *

def main():
    file = input("Enter filename: ")
    infile = open(file)
    win = GraphWin('Grades',300,100)
    y =10
    x =100
    infile.readline()
    for word in infile.readlines():
        name,num = word.split()
        barNum = int(num)
        Text(Point(45,y+5),name).draw(win)
        Rectangle(Point(x,y),Point(x+barNum,y+10)).draw(win)
        y+=20
        
    win.getMouse()
    infile.close()
    win.close()
main()

#C:\\Users\\eliiv\\Downloads\\Year1\\Fall\\CS-101\\Lab Codes\\Lab 5\\ForP5_15.py
