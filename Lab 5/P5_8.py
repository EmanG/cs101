#P5_8
#by:Elijah Iverson

def main():
    word=input("Enter your phrase: ")
    key=2
    abcs="abcdefghijklmnopqrstuvwxyz"
    temp=''
    for n in range(len(word)):
        if(word[n].lower()==abcs[25]):
            temp+=chr(ord(word[n])+key-26)
        if(word[n].lower()==abcs[24]):
            temp+=chr(ord(word[n])+key-26)
        if(word[n].lower()!=abcs[24] and word[n].lower()!=abcs[25]):
            temp+=chr(ord(word[n])+key)
    print(temp)
main()
