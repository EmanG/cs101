#P5_12
#by: Elijah Iverson

def main():
    base = int(input("Enter the initial principal: "))
    periods = int(input("Enter the number of times that the interest is compounded each year: "))
    rate = float(input("Enter the interest rate: "))
    print("Year\tValue\n")
    for i in range(periods):
        base = round(base + rate/periods,2)
        print("{0:3}\t{1}".format(i+1,base))
main()
