#P5_14
#by:Elijah Iverson

def main():
    file = input("Enter filename: ")
    infile = open(file)
    data = infile.read()
    numline = 0
    length = 0
    words = 0
    for line in data:
        for Str in data.split():
            length+=len(Str)
            words+=1
        numline+=1
    print("Lines: {0}\tWords: {1}\tCharacters: {2}".format(numline,length,words))
main()
