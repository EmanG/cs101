#P5_13
#by:Elijah Iverson

def main():
    #get the date from file
    infileName = "C:\\Users\\eliiv\\Downloads\\Year1\\Fall\\CS-101\\Lab Codes\\Lab 5\\ForP5_13.py"
    infile = open(infileName)
    file = infile.read()
    
    #split into components
    monthStr, dayStr, yearStr = file.split("/")

    #convert monthStr to the month name
    months = ["January","February","March","April",
              "May","June","July","August",
              "September","October","November","December"]
    monthStr = months[int(monthStr)-1]

    #output resukts in month day, year format
    print("the converted date from the file is: {0} {1}, {2}".format(monthStr,dayStr,yearStr))

main()
