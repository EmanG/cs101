#P5_11
#by: Elijah Iverson

def main():
    print("This program illustrates a choatic function")
    x = float(input("Enter a number between 0 and 1: "))
    x2 = float(input("Enter another number between 0 and 1: "))
    print("index\t{0}\t{1}\n".format(x,x2))
    for i in range(10):
        x = 3.9 * x * (1-x)
        x2 = 3.9 * x2 * (1-x2)
        print("{0:3}\t{1}\t{2}".format(i+1,round(x,4),round(x2,4)))

main()
    
