#P4_1
#by: Elijah Iverson

from graphics import *

def main():
    win = GraphWin('Bouncy Ball',1000,500)
    shape = Circle(Point(30,30),30)
    shape.setFill("blue")
    shape.draw(win)
    dx = 1
    dy = 1
    for i in range(10000):
        rp = shape.getCenter().getX()+shape.getRadius() #Right Position
        lp = shape.getCenter().getX()-shape.getRadius() #Left Position
        tp = shape.getCenter().getY()+shape.getRadius() #Top Position
        bp = shape.getCenter().getY()-shape.getRadius() #Bottom Position
        if 1000 == rp:
            dx = -1
        elif 0 == lp:
            dx = 1
        elif 0 == bp:
            dy = 1
        elif 500 == tp:
            dy = -1
        shape.move(dx,dy)
        update(30)
    print("Click to quit.")
    win.getMouse()
    win.close()
main()
