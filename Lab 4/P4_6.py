# P4_6

from graphics import * 
def main():  
    print("This program plots the growth of a 10-year investment .") 
    
    ewin = GraphWin("Entry",350,90)
    Text(Point(90,30),"Enter the initial principal: ").draw(ewin)
    Text(Point(120,60),"Enter the annualized interest rate: ").draw(ewin)
    iB1=Entry(Point(300,30),10).draw(ewin)
    iB2=Entry(Point(300,60),10).draw(ewin)
    ewin.getMouse()
    principal=float(iB1.getText())
    apr=float(iB2.getText())
    
    win = GraphWin("Investment Growth Chart", 320, 240)
    win.setBackground("white")
    win.setCoords(-1.75,-200, 11.5, 10400)
    Text(Point(-1, 0) , ' O.OK').draw(win)
    Text(Point(-1, 2500) , ' 2.5K').draw(win)
    Text(Point(-1, 5000) , ' 5.0K').draw(win)
    Text(Point(-1, 7500) , ' 7.5K').draw(win)
    Text(Point(-1, 10000) , '10.0K').draw(win)

    bar = Rectangle(Point(0, 0) , Point (1, principal))
    bar.setFill("green")
    bar.setWidth(2)
    bar.draw(win)

    for year in range(1, 11) :
        principal = principal * (1 + apr)
        bar = Rectangle(Point(year, 0) , Point(year+1, principal))
        bar.setFill ("green")
        bar.setWidth(2)
        bar.draw(win)

    input("Press <Enter> to quit.")
    win. close()

main()
        
