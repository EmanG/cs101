#P4_9
#by: Elijah Iverson

from graphics import *

def main():
    win = GraphWin('Rectangle')
    p1=win.getMouse()
    p2=win.getMouse()
    Rectangle(p1,p2).draw(win)
    area = (abs(p1.getX()-p2.getX()))*(abs(p1.getY()-p2.getY()))
    print(area)
    perimeter=2*(abs(p1.getX()-p2.getX()))-(abs(p1.getY()-p2.getY()))
    print(perimeter)

    win.getMouse()
    win.close()
main()
