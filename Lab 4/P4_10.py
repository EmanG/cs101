#P4_10
#by: Elijah Iverson

from graphics import *
import math

def main():
    win = GraphWin('Triangle')
    p1=win.getMouse()
    p2=win.getMouse()
    p3=win.getMouse()
    Polygon(p1,p2,p3).draw(win)
    dx1=p2.getX()-p1.getX()
    dy1=p2.getY()-p1.getY()
    length1=math.sqrt((dx1**2)+(dy1**2))
    dx2=p3.getX()-p2.getX()
    dy2=p3.getY()-p2.getY()
    length2=math.sqrt((dx2**2)+(dy2**2))
    dx3=p1.getX()-p3.getX()
    dy3=p1.getY()-p3.getY()
    length3=math.sqrt((dx3**2)+(dy3**2))
    s=(length1+length2+length3)/2
    area = math.sqrt(s*(s-length1)*(s-length2)*(s-length3))
    print(area)

    win.getMouse()
    win.close()
main()
