#P4_3
#Face

from graphics import *

def main():
    win = GraphWin("smile",500,500)

    win.setBackground('gray')
    face=Circle(Point(250,250),125)
    face.setFill("white")
    e1=Circle(Point(200,210),30)
    e2=Circle(Point(300,210),30)
    e1.setFill("blue")
    e2.setFill("blue")
    smile=Polygon(Point(175,275),Point(325,275),Point(250,350))
    smile.setFill("red")
    face.draw(win)
    e1.draw(win)
    e2.draw(win)
    smile.draw(win)
    
    win.getMouse()
main()
