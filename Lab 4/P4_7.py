#P4_7
#by: Elijah Iverson

from graphics import *
import math
def main():
    iwin = GraphWin('Input',350,90)#Input Window
    Text(Point(60,30),"Enter the radius: ").draw(iwin)
    Text(Point(75,60),"Enter the Y-intercept: ").draw(iwin)
    iB1=Entry(Point(300,30),10).draw(iwin)#6
    iB2=Entry(Point(300,60),10).draw(iwin)#5
    iwin.getMouse()
    r=float(iB1.getText())*10#60
    y=100-(float(iB2.getText())*10)#50
    #print(r)
    #print(y)

    win = GraphWin('Circle Intersection')#Results Window
    Circle(Point(100,100),r).draw(win)#Circle
    Line(Point(0,y),Point(200,y)).draw(win)#Horizontal Line
    x=(r**2)-(y**2)#1100
    #print(x)
    if(x>=0):
        px=math.sqrt(x)#33.16624
        nx=100-px#66.83375
        px=px+100#133.16624
        x1=Circle(Point(px,y),2)
        x2=Circle(Point(nx,y),2)
        x1.setFill('red')
        x2.setFill('red')
        x1.setOutline('red')
        x2.setOutline('red')
        x1.draw(win)
        x2.draw(win)
        Text(Point(px,y-15),"("+str(round(10-(nx/10),2))+","+str(round(y/10,2))+")").draw(win)
        Text(Point(nx,y+15),"("+str(round(10-(px/10),2))+","+str(round(y/10,2))+")").draw(win)
    else:
        Text(Point(100,y),"(0,0)").draw(win)

    win.getMouse()
    input("Press <Enter> to quit.")
    win.close()
    iwin.close()
main()
