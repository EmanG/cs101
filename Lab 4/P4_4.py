#P4_4
#by: Elijah Iverson

from graphics import *

def main():
    win = GraphWin('Christmas',500,500)

    win.setBackground('blue')
    center = Point(250,250)
    ground = Rectangle(Point(0,300),Point(500,500))
    ground.setFill('white')
    ground.setOutline('white')
    log = Rectangle(Point(75,150),Point(125,350))
    log.setFill('brown')
    log.setOutline('brown')
    gTop = Polygon(Point(50,150),Point(150,150),Point(100,100))
    gTop.setFill('green')
    gTop.setOutline('green')
    gMid = Polygon(Point(25,225),Point(175,225),Point(100,100))
    gMid.setFill('green')
    gMid.setOutline('green')
    gBot = Polygon(Point(0,330),Point(200,330),Point(100,150))
    gBot.setFill('green')
    gBot.setOutline('green')
    sTop = Circle(Point(350,175),25)
    sTop.setFill('white')
    sTop.setOutline('black')
    sMid = Circle(Point(350,233),37.5)
    sMid.setFill('white')
    sMid.setOutline('black')
    sBot = Circle(Point(350,315),50)
    sBot.setFill('white')
    sBot.setOutline('black')
    
    ground.draw(win)
    log.draw(win)
    gTop.draw(win)
    gMid.draw(win)
    gBot.draw(win)
    sBot.draw(win)
    sMid.draw(win)
    sTop.draw(win)
    
    win.getMouse()
    win.close()
main()
    
