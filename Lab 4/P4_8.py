#P4_8
#by:Elijah Iverson

from graphics import *
import math

def main():
    win = GraphWin('Line Segment')
    p1=win.getMouse()
    p2=win.getMouse()
    Line(p1,p2).draw(win)
    dx=p2.getX()-p1.getX()
    dy=p2.getY()-p1.getY()
    slope=dy/dx
    length=math.sqrt((dx**2)+(dy**2))
    rwin = GraphWin('Results',350,90)
    Text(Point(175,30),"Length: " + str(length)).draw(rwin)
    Text(Point(175,60),"Slope: " + str(slope)).draw(rwin)
    
    win.getMouse()
    win.close()
    rwin.close()
main()
