#P8_15
#by: Elijah Iverson

from graphics import *

def main(): 
    infile = input("File name: ")
    image = Image(Point(100,100), infile)
    width = image.getWidth()
    height = image.getHeight()
    win = GraphWin("grayscaled")
    image.draw(win)
    win.getMouse()
    for row in range(height):
        for column in range(width):
            r, g, b = image.getPixel(row, column)
            image.setPixel(row, column, color_rgb(255-r, 255-g, 255-b))
            win.update()
    win.getMouse()
    win.close()
main()
#C:\\Users\\eliiv\\Pictures\\Camera Roll\\gifforP8_15.gif
