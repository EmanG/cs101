#P8_12
#by: Elijah Iverson

def main():
    file = "C:\\Users\\eliiv\\Downloads\\Year1\\Fall\\CS-101\\Lab Codes\\Lab 8\\ForP8_12.py"
    infile = open(file,'r')
    store=0
    i=0
    heat=0
    cool=0
    for line in infile:
        store+=int(line)
        i+=1
    store=round(store/i,3)
    if store<60:
        heat=round(60-store,3)
    elif store>80:
        cool=round(store-80,3)
    if (heat != 0 or cool != 0) or (store>60 or store<80):
        print("The average tempature is {0} with heating degrees of {1} and cooling degrees of {2}".format(store,heat,cool))
main()

