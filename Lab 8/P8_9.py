#P8_9
#by: Elijah Iverson

def main():
    q=0
    MPG=[]
    while q==0:
        odometer=float(input("Enter the current odometer reading (distance in miles) >>> "))
        gas=float(input("Enter the amount of gas used (gallons) >>> "))
        mpg=odometer/gas
        MPG.append(mpg)
        repeat=input("\nPress <Enter> to quit, or type anything else then <Enter> to continue >>> ")
        if repeat == "":
            q+=1
    print(MPG)
main()
