#P8_14
#by: Elijah Iverson

from graphics import *

def main(): 
    infile = input("File name: ")
    image = Image(Point(100,100), infile)
    width = image.getWidth()
    height = image.getHeight()
    win = GraphWin("grayscaled")
    image.draw(win)
    win.getMouse()
    for row in range(200):
        for column in range(200):
            r, g, b = image.getPixel(row, column)
            brightness = int(round(0.299 * r + 0.587 * g + 0.114 * b))
            image.setPixel(row, column, color_rgb(brightness, brightness, brightness))
            win.update()
    win.getMouse()
    win.close()
main()
#C:\\Users\\eliiv\\Pictures\\Camera Roll\\gifforP8_14.gif
    
