#P8_1
#by: Elijah Iverson

def fib(n):
    first = 1
    second = 0
    third = 0
    for i in range(1,n+1):
        third = first + second
        first = second
        second = third
    return third

def main():
    num = int(input("Enter the nth number you want from the Fibonacci Sequence >> "))
    print(fib(num))
main()
