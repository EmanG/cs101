#P8_8
#by: Elijah Iverson

def main():
    n=float(input("Enter the first number for greatest common divisor >>> "))
    m=float(input("Enter the second number for greatest common divisor >>> "))
    while m > 0:
        t=n
        n=m
        m=t%m
    print("The Greatest Common Divisor is",n)
main()
