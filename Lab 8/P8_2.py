#P8_2
#by: Elijah Iverson

def main():
    wind = [0,5,10,15,20,25,30,35,40,45,50]#Row
    temp = [-20,-10,0,10,20,30,40,50,60]#Column
    prnt = []
    string = ""
    for V in wind:
        for T in temp:
            results = (35.74 + (0.6215*T)-(35.75*(V**0.16))+(0.4275*T*(V**0.16)))
            prnt.append(round(results,2))
        for i in range(len(prnt)):
            string += str(prnt[i]) + "\t"
        print(string)
        prnt = []
        string = ""
main()
