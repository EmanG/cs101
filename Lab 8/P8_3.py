#P8_3
#by: Elijah Iverson

def main():
    init = float(input("Enter your initial investment >> "))
    rate = float(input("Enter your interest rate >> "))
    time = 0
    double = float(init * 2)
    while init != double and init < double:
        init += (init*rate)
        time += 1
    print("It'll take",time,"years to double the initial investment amount with the interest rate given.")
main()
