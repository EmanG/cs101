#P8_6
#by: Elijah Iverson

from math import *

def main():
    n = int(input("Enter a number to find out if its prime >>> "))
    t=0
    prime = []
    if n>2:
        while n>2:
            half = int(sqrt(n))
            for i in range(2, half+1):
                if((n%i)==0):
                    t+=1
            if t==0:
                prime.append(n)
            n-=1
            t=0
    print(prime) 
main()
