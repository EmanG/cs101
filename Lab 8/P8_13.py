#P8_13
#by: Elijah Iverson

from graphics import *
from math import *

def main():
    win = GraphWin("P8_13",500,500)
    doneBt=Rectangle(Point(5,445),Point(55,495))
    doneBt.setFill('red')
    donetxt=Text(Point(30,470),"Done.")
    donetxt.setStyle('bold')
    doneBt.draw(win)
    donetxt.draw(win)
    t=0
    points=[]
    count=-1
    while t==0:
        points.append(win.getMouse())
        count+=1
        temp=points[count]
        if (temp.getX()>5 and temp.getY()>445)and(temp.getX()<55 and temp.getY()<495):
            t+=1
        else:
            Circle(temp,3).draw(win)
    X=0
    Y=0
    XP=0
    XY=0
    n=0
    for p in points:
        X+=p.getX()
        Y+=p.getY()
        XP+=(p.getX()**2)
        XY+=(p.getX()*p.getY())
        n+=1
    X/=n
    Y/=n
    m=(XY-(n*X*Y))/(XP-(n*(X**2)))
    y1=Y+(m*(0-X))
    x1=((0-Y)/m)+X
    y2=Y+(m*(500-X))
    x2=((500-Y)/m)+X
    Line(Point(x1,y1),Point(x2,y2)).draw(win)
    win.getMouse()
    win.close()
main()
