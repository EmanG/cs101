#P8_7
#by: Elijah Iverson

from math import *

def main():
    even = int(input("Enter an even number >>> "))
    t=0
    if(even%2)==0:
        p = prime(even)
        n = even-p
    else:
        main()
    print("The primes numbers {0} and {1} equal {2}".format(n,p,even))

def prime(n):
    t=0
    temp=n
    if n>2:
        while n>2:
            half = int(sqrt(n))
            for i in range(2, half+1):
                if((n%i)==0):
                    t+=1
            if t==0 and n < (temp-2):
                return n
            n-=1
            t=0
main()
