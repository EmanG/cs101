#P6_9
#by: Elijah Iverson

def main():
    score=input("Enter your score from the exam (0-100): ")
    score=grading(score)
    print("Your Letter grade for the quiz is: {0}".format(score))

def grading(score):
    grades=['F','D','C','B','A']
    s=int(score)
    if(s<60):
        score=grades[0]
    if(59<s<70):
        score=grades[1]
    if(69<s<80):
        score=grades[2]
    if(79<s<90):
        score=grades[3]
    if(89<s<101):
        score=grades[4]
    return score

main()
