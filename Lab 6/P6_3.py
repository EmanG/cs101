#P6_3
#by: Elijah Iverson

import math

def sphereArea(radius):
    A = 4 * math.pi * (radius ** 2)
    return A

def sphereVolume(radius):
    V = (4/3)*math.pi*(radius**3)
    return V

def main():
    print("This program calculates the volume and area of a sphere.")
    r = eval(input("Enter the radius: "))
    v = round(sphereVolume(r),3)
    a = round(sphereArea(r),3)
    print("The volumue of the sphere is:",v,"and the surface area is:",a)

main()
