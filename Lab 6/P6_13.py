#P6_13
#Changing the letters in a string to unicode
#and adding the unicode from one string together
#for one entry in the return list
#by: Elijah Iverson

def toNumbers(strList):
    nums = []
    temp = 0
    for Str in strList:
        for let in range(len(Str)):
            temp += ord(Str[let])
        nums.append(temp)
        temp = 0
    return nums
def main():
    strList = ['string','cant','change']
    nums = toNumbers(strList)
    print(nums)
main()
