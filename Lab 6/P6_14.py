#P6_14
#by: Elijah Iverson

def sumSquare(nums):
    total = 0
    for num in nums.split():
        total += int(num)**2
    return total
def main():
    file = input("Enter filename: ")
    infile = open(file)
    total =0
    for line in infile.readlines():
        total+= sumSquare(line)
    print(total)
main()

#C:\\Users\\eliiv\\Downloads\\Year1\\Fall\\CS-101\\Lab Codes\\Lab 6\\ForP6_14.py
