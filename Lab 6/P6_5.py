#P6_5
#by: Elijah Iverson

import math

def main():
    print("This program calculates the cost per square inch of a circular pizza")
    p = eval(input("Enter the price of the pizza: "))
    d = eval(input("Enter the size of the pizza: "))
    r = pizzaCost(p,d)
    print("The pizza cost",r," per square inch")

def pizzaArea(d):
    return math.pi*((d/2)**2)

def pizzaCost(p,d):
    return round(p/pizzaArea(d),2)

main()
