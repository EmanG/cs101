#P6_17
#by: Elijah Iverson

from graphics import *
import math

def moveTo(shape, newCenter):
    x = shape.getCenter()
    x1 = x.getX()
    x2 = newCenter.getX()
    y = shape.getCenter()
    y1 = y.getY()
    y2 = newCenter.getY()
    dx = x2 - x1
    dy = y2 - y1
    shape.move(dx,dy)

def main():
    win = GraphWin()
    c = Rectangle(Point(75,75),Point(125,125))
    c.setFill('blue')
    c.draw(win)
    for i in range(10):
        newC = win.getMouse()
        moveTo(c,newC)
        
    win.getMouse()
    win.close()
main()
    
