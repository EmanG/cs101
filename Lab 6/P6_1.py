#P6_1
#by: Elijah Iverson

def macdonald():
    print("Old MacDonald had a farm, Ee-igh, Ee-igh, Oh!")

def cow():
    macdonald()
    print("And on that farm he had a cow, Ee-igh, Ee-igh, Oh!")
    print("With a moo, moo here and a moo, moo there.")
    print("Here a moo, there a moo, everywhere a moo, moo.")
    macdonald()
    print()

def chicken():
    macdonald()
    print("And on that farm he had a chicken, Ee-igh, Ee-igh, Oh!")
    print("With a cluck, cluck here and a cluck, cluck there.")
    print("Here a cluck, there a cluck, everywhere a cluck, cluck.")
    macdonald()
    print()

def dog():
    macdonald()
    print("And on that farm he had a dog, Ee-igh, Ee-igh, Oh!")
    print("With a bark, bark here and a bark, bark there.")
    print("Here a bark, there a bark, everywhere a bark, bark.")
    macdonald()
    print()

def cat():
    macdonald()
    print("And on that farm he had a cat, Ee-igh, Ee-igh, Oh!")
    print("With a meow, meow here and a meow, meow there.")
    print("Here a meow, there a meow, everywhere a meow, meow.")
    macdonald()
    print()

def sheep():
    macdonald()
    print("And on that farm he had a sheep, Ee-igh, Ee-igh, Oh!")
    print("With a baa, baa here and a baa, baa there.")
    print("Here a baa, there a baa, everywhere a baa, baa.")
    macdonald()
    print()

def main():
    cow()
    chicken()
    dog()
    cat()
    sheep()
main()
