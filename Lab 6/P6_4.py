#P6_4
#by: Elijah Iverson

def sumN(n):
    N=0
    for i in range(1,n+1):
        N+=i
    return N

def sumNCubes(n):
    N=0
    for i in range(1,n+1):
        N+=i**3
    return N

def main():
    iput = input("Enter a natural number: ")
    num = int(iput)
    nSum=sumN(num)
    nCube=sumNCubes(num)
    print("The sum of first",num,"natural numbers is",nSum,"and the cube sum of them are",nCube)

main()
