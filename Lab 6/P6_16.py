#P6_16
#by: Elijah Iverson

from graphics import *
import math

def drawFace(center,size,win):
    face = Circle(center,size)
    lefti = Circle(Point(center.getX()-size/3,center.getY()-size/3),size/6)
    righti = Circle(Point(center.getX()+size/3,center.getY()-size/3),size/6)
    mouth = Polygon(Point(center.getX()-size/3,center.getY()+size/4),Point(center.getX()+size/3,center.getY()+size/4),Point(center.getX()+size/6,center.getY()+size/2),Point(center.getX()-size/6,center.getY()+size/2))
    face.setFill('white')
    lefti.setFill('blue')
    righti.setFill('blue')
    mouth.setFill('red')
    face.draw(win)
    lefti.draw(win)
    righti.draw(win)
    mouth.draw(win)

def main():
    win = GraphWin()
    img = Image(Point(100,100),"ForP6_16.png")
    img.draw(win)
    num = int(input("Enter how many faces there are in the picture: "))
    for i in range(num):
        if i < 1:
            print("Click the center of the face then the edge of the face.")
        else:
            print("Again on the next face.")
        center = win.getMouse()
        edgeP = win.getMouse()
        edge = math.sqrt(((center.getX()-edgeP.getX())**2)+((center.getY()-edgeP.getY())**2))
        drawFace(center,edge,win)
    win.getMouse()
    win.close()
main()
