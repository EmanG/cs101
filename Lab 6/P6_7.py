#P6_7
#Fibonacci Sequence
#by:Elijah Iverson

import math

def main():
    n=eval(input("Enter a number for Fibonacci Sequence: "))
    results = fib(n)
    print("The Fibonacci Sequence number with the index",n,"is",results)

def fib(n):
    t=1
    o=0
    p=0
    for i in range(n):
        t=t+p
        p=o
        o=t
    return t

main()
