#P6_11
#by: Elijah Iverson

def squareEach(nums):
    i = 0
    for num in nums:
        num = num **2
        nums[i] = num
        i+=1
    return nums
def main():
    nums = [1,2,3,6,9]
    nums = squareEach(nums)
    print(nums)
main()
