#P6_8
#by:Elijah Iverson

import math

def main():
    x=eval(input("Enter your guess value: "))
    n=eval(input("Enter the number of times you want to improve the guess: "))
    g=nextGuess(x,n)
    print("The final value is:",g)
    t=101-(g/math.sqrt(x))
    print("The accuracy is:",t,"%")

def nextGuess(guess, x):
    g=guess/2
    for i in range(x):
        g=(g+(guess/g))/2
    return g

main()
