#P6_15
#by: Elijah Iverson

from graphics import *

def drawFace(center,size,win):
    Circle(center,size).draw(win)
    Circle(Point(center.getX()-size/3,center.getY()-size/3),size/6).draw(win)
    Circle(Point(center.getX()+size/3,center.getY()-size/3),size/6).draw(win)
    Polygon(Point(center.getX()-size/3,center.getY()+size/4),Point(center.getX()+size/3,center.getY()+size/4),Point(center.getX()+size/6,center.getY()+size/2),Point(center.getX()-size/6,center.getY()+size/2)).draw(win)

def main():
    size = float(input("Enter the face's size: "))
    win = GraphWin()
    center = Point(100,100)
    drawFace(center,size,win)
    win.getMouse()
    win.close()
main()
