#P6_6
#by: Elijah Iverson

import math

def main():
    a = eval(input("Enter the a side: "))
    b = eval(input("Enter the b side: "))
    c = eval(input("Enter the c side: "))
    x = triArea(a,b,c)
    print("This is the area of the triangle: ",x)

def triArea(x,y,z):
    s = (x+y+z)/2
    return round(math.sqrt((s*((s-x)*(s-y)*(s-z)))),2)

main()
