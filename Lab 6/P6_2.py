#P6_2
#by: Elijah Iverson

def marching(num):
    word = ['zero','one','two','three','four','five','six','seven','eight','nine','ten','eleven','twelve']
    print("The ants go marching", word[num], "by", word[num] + ", hurrah! hurrah!")
    print("The ants go marching", word[num], "by", word[num] + ", hurrah! hurrah!")
    print("The ants go marching", word[num], "by", word[num] + ", ")

def little(phrase):
    print("The little one stops to", phrase + ",")

def boom():
    print("And they all go marching down ...")
    print("In the ground ...")
    print("To get out ....")
    print("Of the rain. ")
    print("Boom! Boom! Boom!")

def main():
    phrases = ["","suck his thumb","tie his shoe","lick his hand","look for his hat","suck on his other thumb","tie his other shoe","lick his other hand","look for his glove","search for his nose","sneeze loudly","cough away sickness"]
    for i in range(1,11):
        marching(i)
        little(phrases[i])
        boom()

main()
        
    
    
