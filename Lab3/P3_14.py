#P3_14
#a program that finds the average of a series of numbers entered by user.
#by: Eliajh Iverson

def main():
    n=eval(input("Enter the amount of numbers you want to average: "))
    t=0
    for i in range(n):
        x=eval(input("Enter number: "))
        t=t+x
    t=t/n
    print("Your average is",t)
