#P3_4
#   A program that calculates the distance of a lightning strike
#by: Elijah Iverson

def main():
    print("This program calculates the distance of a lightning strike")
    t = eval(input("Enter how many seconds the sound of thunder came after the flash: "))
    d = (1100 * t)/5280
    print("The distance of the lightning strike is:",d,"miles away")
