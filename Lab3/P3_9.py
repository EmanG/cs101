#P3_9
# a program to calculate the area of a triangle
#by: Elijah Iverson

import math

def main():
    print("This program calculates the area of a triangle with given length for all three sides")
    a = eval(input("Enter the a side: "))
    b = eval(input("Enter the b side: "))
    c = eval(input("Enter the c side: "))
    s = (a+b+c)/2
    x = round(math.sqrt((s*((s-a)*(s-b)*(s-c)))),2)
    print("This is the area of the triangle: ",x)
