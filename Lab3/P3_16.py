#P3_16
#Fibonacci Sequence
#by:Elijah Iverson

import math

def main():
    n=eval(input("Enter a number for Fibonacci Sequence: "))
    t=1
    o=0
    p=0
    for i in range(n):
        t=t+p
        p=o
        o=t
    print("The Fibonacci Sequence number with the index",n,"is",t)
