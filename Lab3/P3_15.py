#P3_15
#a program that approximates the value of pi by summing the terms
#by: Elijah Iverson

import math

def main():
    n=eval(input("Enter your number of terms: "))
    t=0
    f=-4
    o=1;
    for i in range(n):
        f=f*-1
        t=t+(f/o)
        o=o+2
    r=t-math.pi
    print("Accuracy is:",r)
