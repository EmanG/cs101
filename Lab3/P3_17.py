#P3_17
#implements newton's method
#by:Elijah Iverson

import math

def main():
    x=eval(input("Enter your guess value: "))
    n=eval(input("Enter the number of times you want to improve the guess: "))
    g=x/2
    for i in range(n):
        g=(g+(x/g))/2
    print("The final value is:",g)
    t=g-math.sqrt(x)
    print("The accuracy is:",t)
