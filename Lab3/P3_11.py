#P3_11
#a program to find the sum of the first n natural numbers
#by: Elijah Iverson

import math

def main():
    n=eval(input("Enter an integer: "))
    c=1
    for i in range(1,n):
        c=c+i+1
    print("The sum of the first",n,"numbers is",c)
