#P3_10
#a program that determines the length of a ladder
#by:Elijah Iverson

import math

def main():
    h=eval(input("Enter the height of the house: "))
    d=eval(input("Enter the angle in degrees of the ladder to the house: "))
    r = (math.pi/180)*d
    l=h/(math.sin(r))
    print("The length of the ladder is",l)
           
