#P3_7
#   A program that calculates the distance between two points.
#by: Elijah Iverson

import math

def main():
    print("This program calculates the distance between two points")
    x1=eval(input("Enter the first x coordinates: "))
    y1=eval(input("Enter the first y coordinates: "))
    x2=eval(input("Enter the second x coordinates: "))
    y2=eval(input("Enter the second y coordinates: "))
    distance=math.sqrt(((x2-x1)**2)+((y2-y1)**2))
    print("The distances between the two points",distance)
