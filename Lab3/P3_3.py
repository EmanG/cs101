#P3_3
#   A program that computes the molecular weight of atoms
#by: Elijah Iverson

def main():
    print("This program computes the molecular weight of a carbonhydrate")
    h = eval(input("Enter the amount of hydrogen atoms: "))
    c = eval(input("Enter the amount of carbon atoms: "))
    o = eval(input("Enter the amount of oxygen atoms: "))
    w = round((h*1.00794)+(c*12.0107)+(o*15.9994),4)
    print("The carbonhydrate's molecular weight is",w,"grams/mole")
    
