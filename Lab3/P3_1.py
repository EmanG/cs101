# P3_1.py
#   A program that calculates volume and area of a sphere.
#by: Elijah Iverson

import math
def main():
    print("This program calculates the volume and area of a sphere.")
    r = eval(input("Enter the radius: "))
    v = round((4/3) * math.pi * (r**3),3)
    a = round(4 * math.pi * (r**2),3)
    print("The volumue of the sphere is:",v," and the area is:",a)
