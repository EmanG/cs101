#P3_5
#   A program that calculates the orders cost
#by: Elijah Iverson

def main():
    print("This program calculates the cost of the coffee order from Konditorei")
    p = eval(input("Enter the amount of pounds of coffee you'd like to order: "))
    c = round((10.50*p)+(0.86*p)+1.50,2)
    print(p,"lbs of coffee cost:",c,"dollars")
