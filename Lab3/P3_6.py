#P3_6
#   A program to calculate the slope of two points
#by: Elijah Iverson

def main():
    print("This program calculates the slope of two points")
    x1=eval(input("Enter the first x coordinates: "))
    y1=eval(input("Enter the first y coordinates: "))
    x2=eval(input("Enter the second x coordinates: "))
    y2=eval(input("Enter the second y coordinates: "))
    slope=(y2-y1)/(x2-x1)
    print("The slope of the coordinates is", slope)
