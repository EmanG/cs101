#P3_2.py
#   A program that calculates the cost per square inch of a circular pizza
#by: Elijah Iverson

import math

def main():
    print("This program calculates the cost per square inch of a circular pizza")
    p = eval(input("Enter the price of the pizza: "))
    d = eval(input("Enter the size of the pizza: "))
    r = round(p/(math.pi*((d/2)**2)),2)
    print("The pizza cost",r," per square inch")
    
