#P7_4
#by: Elijah Iverson

def main():
    c = int(input("Enter your number of credits you have: "))
    if c < 7 and  c >= 0:
        print("You're a freshman!")
    elif c < 16 and c >= 7:
        print("You're a sophomore!")
    elif c < 26 and c >= 16:
        print("You're a junior!")
    elif c >= 26:
        print("You're a senior!")
    else:
        print("invalid number of credits!")
main()
