#P7_9
#by:Elijah Iverson

def main():
    year = int(input("Enter a year thats between 1982-2048 >> "))
    while year < 1982 or year > 2048:
        print("Out of range!")
        year = int(input("Enter a new year thats between 1982-2048 >> "))
    a = year%19
    b = year%4
    c = year%7
    d = ((19*a) + 24)%30
    e = ((2*b) + (4*c) + (6*d) + 5)%7
    month = ['March','April']
    monthi = 0
    day = 22 + d + e
    if day > 31:
        monthi+=1
        day-=31
    print("Easter will be on {0} {1} that year.".format(month[monthi],day))

main()

    
