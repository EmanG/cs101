#P7_11
#by: Elijah Iverson

def main():
    year = int(input("Enter a year (yyyy) >> "))
    if((year%4)==0 and (year%400)!=0):
        if(year%100)!=0:
            print("{0} is a leap year.".format(year))
        else:
            print("{0} is NOT a leap year".format(year))
    elif(year%400)==0:
        print("{0} is a leap year.".format(year))
    else:
        print("{0} is NOT a leap year.".format(year))

main()
