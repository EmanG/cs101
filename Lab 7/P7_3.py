#P7_3
#by: Elijah Iverson

def main():
    g = float(input("Enter your exam grade in the scale of 0-100: "))
    if g <= 100 and g >= 90:
        print("Your grade is an A")
    elif g < 90 and g >= 80:
        print("Your grade is a B")
    elif g < 80 and g >= 70:
        print("Your grade is a C")
    elif g < 70 and g >= 60:
        print("Your grade is a D")
    elif g < 60 and g >= 0:
        print("Your grade is a F")
    else:
        print("Thats not a valid exam grade in the scale of 0 - 100!")
main()
