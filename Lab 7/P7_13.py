#P7_13
#by: Elijah Iverson

def main():
    date = input("Enter a date to vaildate (MM/DD/YYYY) >> ")
    daysINmonth = [0,31,28,31,30,31,30,31,31,30,31,30,31]
    newdate = date.split('/')
    month = int(newdate[0])
    day = int(newdate[1])
    year = int(newdate[2])
    total = 0
    if leap(year) == 1:
        daysINmonth[2] = 29
    if 0 < month < 13:
        if 0 < day < daysINmonth[month]:
            for i in range(1,month+1):
                total += daysINmonth[i]
            print("Total number of days: {0}".format(total))
        else:
            print("The date {0} is NOT valid.".format(date))
    else:
        print("The date {0} is NOT valid.".format(date))

def leap(year):
    if((year%4)==0 and (year%400)!=0):
        if(year%100)!=0:
            return 1
        else:
            return 0
    elif(year%400)==0:
        return 1
    else:
        return 0

main()    
