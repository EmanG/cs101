#P7_16
#by: Elijah Iverson

from graphics import *
import math

def drawTarget(win,size):
    center = Point((size/2),(size/2))
    circle1 = Circle(center,100)
    circle1.setFill("white")
    circle2 = Circle(center, 80)
    circle2.setFill("black")
    circle3 = Circle(center, 60)
    circle3.setFill("blue")
    circle4 = Circle(center, 40)
    circle4.setFill("red")
    circle5 = Circle(center, 20)
    circle5.setFill("yellow")
    circle1.draw(win)
    circle2.draw(win)
    circle3.draw(win)
    circle4.draw(win)
    circle5.draw(win)
    return center

def shootArrow(win,center):
    arrow = win.getMouse()
    x1 = center.getX()
    y1 = center.getY()
    x2 = arrow.getX()
    y2 = arrow.getY()
    distance = math.sqrt(((x2-x1)**2)+((y2-y1)**2))
    return distance

def main():
    s = 500
    win = GraphWin('target',s,s)
    c = drawTarget(win,s)
    d = shootArrow(win,c)
    if 80 < d <= 100:
        print("Scored 1 point!")
    elif 60 < d <= 80:
        print("Scored 3 points!")
    elif 40 < d <= 60:
        print("Scored 5 points!")
    elif 20 < d <= 40:
        print("Scored 7 points!")
    elif 0 < d <= 20:
        print("Scored 9 points!")
    else:
        print("Scored nothing")

    win.getMouse()
    win.close()
main()
    
    
