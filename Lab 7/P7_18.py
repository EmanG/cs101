#P7_18
#Base off of P5_2.py
#by: Elijah Iverson

def main():
    grades=['F','F','D','C','B','A']
    loop = 1
    wrong = 0
    while loop==1:
        score=input("Enter your score from the quiz (0-5): ")
        if score == '1' or score == '2' or score == '3' or score == '4' or score == '5':
            score=grades[int(score)]
            print("Your Letter grade for the quiz is: {0}".format(score))
        else:
            wrong+=1
        if wrong == 0:
            score=input("Do you have moer quiz scores to input?\nIf you do type: \"yes\"\n")
        if score[0] != 'y':
            loop+=1
    
main()
