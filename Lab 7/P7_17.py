#P7_17
#by: Elijah Iverson

from graphics import *

def main():
    win = GraphWin('Bouncy Ball',600,300)
    shape = Circle(Point(30,30),30)
    shape.setFill("blue")
    shape.draw(win)
    dx = 1
    dy = 1
    while 1==1:
        rp = shape.getCenter().getX()+shape.getRadius() #Right Position
        lp = shape.getCenter().getX()-shape.getRadius() #Left Position
        tp = shape.getCenter().getY()-shape.getRadius() #Top Position
        bp = shape.getCenter().getY()+shape.getRadius() #Bottom Position
        if 0 == lp and 300 == bp:
            dx = 1
            dy = -1
        elif 0 == lp and 0 == tp:
            dx = 1
            dy = 1
        elif 600 == rp and 300 == bp:
            dx = -1
            dy = -1
        elif 600 == rp and 0 == tp:
            dx = -1
            dy = 1
        elif 600 == rp:
            dx = -1
        elif 0 == lp:
            dx = 1
        elif 0 == tp:
            dy = 1
        elif 300 == bp:
            dy = -1
        shape.move(dx,dy)
        update(30)
    print("Click to quit.")
    win.getMouse()
    win.close()
main()
