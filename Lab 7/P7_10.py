#P7_10
#by: Elijah Iverson

def main():
    year = int(input("Enter a year thats between 1900-2099 >> "))
    while year < 1900 or year > 2099:
        print("Out of range!")
        year = int(input("Enter a new year thats between 1900-2099 >> "))
    a = year%19
    b = year%4
    c = year%7
    d = ((19*a) + 24)%30
    e = ((2*b) + (4*c) + (6*d) + 5)%7
    month = ['March','April']
    monthi = 0
    day = 22 + d + e
    if year == 1954 or year == 1981 or year == 2049 or year == 2076:
        day-=7
        
    if day > 31:
        monthi+=1
        day-=31
    print("Easter will be on {0} {1} {2}.".format(month[monthi],day,year))

main()
