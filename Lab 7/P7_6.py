#P7_6
#by: Elijah Iverson

def main():
    limit = int(input("Enter the speed limit (mph): "))
    speed = float(input("Enter your clocked speed (mph): "))
    if limit < speed:
        if 90 <= speed:
            fine = 200
        else:
            fine = 0
        fine += 5*(speed-limit)
        print("Your fine for speeding over the speed limit is:", fine,"dollars.")
    else:
        print("Your clocked speed is legal.")
main()
