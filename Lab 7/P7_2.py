#P7_2
#by: Elijah Iverson

def main():
    g = int(input("Enter your quiz grade in the scale of 1 - 5: "))
    if g == 5:
        print("Your grade is an A")
    elif g == 4:
        print("Your grade is a B")
    elif g == 3:
        print("Your grade is a C")
    elif g == 2:
        print("Your grade is a D")
    elif g == 1 or g == 0:
        print("Your grade is a F")
    else:
        print("Thats not a valid quiz grade in the scale of 1 - 5!")
main()
