#P7_7
#by: Elijah Iverson

def main():
    print("Military Time. (00:00)-(23:59)")
    start = int(input("Enter the start time (00:00)-(23:59): "))
    end = int(input("Enter the end time (00:00)-(23:59): "))
    totalh = end - start
    charge = 0
    if start <= 2100:
        dayh = 2100 - start
        charge += (2.50 * (dayh*0.01))
        totalh -= dayh
    charge += (1.75 * (totalh*0.01))
    print("Your payment is {:0.2f}".format(charge))
main()
