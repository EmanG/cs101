#P7_8
#by: Elijah Iverson

def main():
    age = int(input("Enter your age (Years only): "))
    cShip = int(input("Enter the number of years you've been a citizen: "))
    if age >= 30 and cShip >= 9:
        print("You can be an US senator or a representative!")
    elif age >= 25 and cShip >= 7:
        print("You can only be an US representative.")
    else:
        print("You aren't eligible to be in the US Senate or House.")
main()
