#P7_5
#by: Elijah Iverson

def main():
    w = float(input("Enter your weight in pounds: "))
    h = float(input("Enter your height in inches: "))
    bmi=(w*720)/(h**2)
    if bmi>25:
        print("You're above the healthy range of the body mass index")
    elif bmi<19:
        print("You're below the healthy range of the body mass index")
    else:
        print("You're in the healthy range of the body mass index")
main()
